//
//  User+CoreDataClass.swift
//  TestCoreData
//
//  Created by Joseph Bryan Baterna on 02/04/2019.
//  Copyright © 2019 Joseph Bryan Baterna. All rights reserved.
//
//

import Foundation
import CoreData

@objc(User)
public class User: NSManagedObject {
    
}
