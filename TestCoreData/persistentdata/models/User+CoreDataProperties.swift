//
//  User+CoreDataProperties.swift
//  TestCoreData
//
//  Created by Joseph Bryan Baterna on 02/04/2019.
//  Copyright © 2019 Joseph Bryan Baterna. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }
    
    @nonobjc public class func fetchUser(email: String) -> NSFetchRequest<User> {
        let request = NSFetchRequest<User>(entityName: "User")
        request.predicate = NSPredicate(format: "email == %@", email)
        return request
    }
    
    @nonobjc public class func deleteUser(email: String) -> Bool {
        let request = NSFetchRequest<User>(entityName: "User")
        request.predicate = NSPredicate(format: "email == %@", email)
        
        do {
            let users = try PersistentService.context.fetch(request)
            for user  in users {
                PersistentService.context.delete(user)
            }
            
            try PersistentService.context.save()
            
            return true
        } catch {
            print("Error Delete")
        }
        
        return false
    }

    @NSManaged public var name: String?
    @NSManaged public var email: String?

}
