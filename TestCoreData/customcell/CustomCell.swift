//
//  CustomCell.swift
//  TestCoreData
//
//  Created by Joseph Bryan Baterna on 02/04/2019.
//  Copyright © 2019 Joseph Bryan Baterna. All rights reserved.
//

import Foundation
import UIKit

class CustomCell: UITableViewCell {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
    }
}
