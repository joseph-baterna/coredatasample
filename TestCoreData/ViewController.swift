//
//  ViewController.swift
//  TestCoreData
//
//  Created by Joseph Bryan Baterna on 02/04/2019.
//  Copyright © 2019 Joseph Bryan Baterna. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITextFieldDelegate {

    var users = [User]()
    
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var myTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textName.delegate = self
        self.textEmail.delegate = self
        
        self.myTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        self.reloadData()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.textName.resignFirstResponder()
        self.textEmail.resignFirstResponder()
        return false
    }

    @IBAction func addUser(_ sender: Any) {
        let name = self.textName.text
        let email = self.textEmail.text
    
        if name?.isEmpty == true || email?.isEmpty == true {
            let alert = UIAlertController(title: "Warning", message: "Enter Name and Email", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let user = User(context: PersistentService.context)
        user.name = name
        user.email = email
        
        PersistentService.saveContext()
        
        users.append(user)
        myTableView.reloadData()
    }
    
    @IBAction func searchUser(_ sender: Any) {
        let email = self.textEmail.text
        
        if email?.isEmpty == true {
            let alert = UIAlertController(title: "Warning", message: "Enter Email", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let request: NSFetchRequest<User> = User.fetchUser(email: email!)
        
        do {
            self.users = try PersistentService.context.fetch(request)
            
            self.myTableView.reloadData()
        } catch {
            print("ERROR GET USER")
        }
        
    }
    
    @IBAction func deleteUser(_ sender: Any) {
        let email = self.textEmail.text
        
        if email?.isEmpty == true {
            let alert = UIAlertController(title: "Warning", message: "Enter Email", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let delete = User.deleteUser(email: email!)
        if delete == true {
            print("Deleted")
            self.reloadData()
        } else {
            print("Failed to delete")
        }
    }
}

extension ViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomCell
        cell.userName.text = users[indexPath.row].name
        cell.userEmail.text = users[indexPath.row].email
        return cell
    }
    
    private func reloadData() {
        let fetchRequest: NSFetchRequest<User> = User.fetchRequest()
        
        do {
            self.users = try PersistentService.context.fetch(fetchRequest)
            self.myTableView.reloadData()
        } catch {
            print("Error")
        }
    }
}

